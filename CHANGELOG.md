Under development
-----------------

2.1.0 (2022-03-28)
-----------------
- Feature: Queue checker should push it's own job, because we cannot be sure that there are any jobs added to queue constantly (mikk150)
- Feature: Add redis support (mikk150)

2.0.0 (2021-11-18)
-----------------
- Feature: One component can add multiple checks (mikk150)
- Feature: Fork it to our repo (mikk150)

1.0.1 (2019-11-12)
-----------------
- Bug: [#1] `Module::$defaultRoute` has to be string (alexeevdv)

1.0.0 (2019-07-11)
-----------------
- Feature: Database component added (alexeevdv)
- Feature: Queue component added (alexeevdv)
