<?php

namespace tests\unit;

use Codeception\Test\Unit;
use trafficcontrol\yii\health\Check;
use trafficcontrol\yii\health\ComponentInterface;
use trafficcontrol\yii\health\Response;
use trafficcontrol\yii\health\ResponseFormatter;

class ResponseFormatterTest extends Unit
{
    public function testStatusIsFailIfOneOfComponentsIsFailed()
    {
        $response = $this->make(Response::class, [
            'data' => [
                $this->makeEmpty(ComponentInterface::class, [
                    'getChecks' => [
                        new Check([
                            'status' => ComponentInterface::STATUS_PASS
                        ])
                    ]
                ]),
                $this->makeEmpty(ComponentInterface::class, [
                    'getChecks' => [
                        new Check([
                            'status' => ComponentInterface::STATUS_FAIL
                        ])
                    ]
                ]),
                $this->makeEmpty(ComponentInterface::class, [
                    'getChecks' => [
                        new Check([
                            'status' => ComponentInterface::STATUS_PASS
                        ])
                    ]
                ]),
            ],
        ]);
        (new ResponseFormatter)->format($response);
        $this->assertEquals('fail', $response->data['status']);
        $this->assertEquals(503, $response->statusCode);
    }

    public function testStatusIsFailIfOneOfComponentsOneCheckIsFailed()
    {
        $response = $this->make(Response::class, [
            'data' => [
                $this->makeEmpty(ComponentInterface::class, [
                    'getChecks' => [
                        new Check([
                            'status' => ComponentInterface::STATUS_PASS
                        ])
                    ]
                ]),
                $this->makeEmpty(ComponentInterface::class, [
                    'getChecks' => [
                        new Check([
                            'status' => ComponentInterface::STATUS_PASS
                        ]),
                        new Check([
                            'status' => ComponentInterface::STATUS_FAIL
                        ]),
                    ]
                ]),
                $this->makeEmpty(ComponentInterface::class, [
                    'getChecks' => [
                        new Check([
                            'status' => ComponentInterface::STATUS_PASS
                        ])
                    ]
                ]),
            ],
        ]);
        (new ResponseFormatter)->format($response);
        $this->assertEquals('fail', $response->data['status']);
        $this->assertEquals(503, $response->statusCode);
    }

    public function testStatusIsFailIfOneOfComponentsIsFailedWhileOtherIsWarn()
    {
        $response = $this->make(Response::class, [
            'data' => [
                $this->makeEmpty(ComponentInterface::class, [
                    'getChecks' => [
                        new Check([
                            'status' => ComponentInterface::STATUS_PASS
                        ])
                    ]
                ]),
                $this->makeEmpty(ComponentInterface::class, [
                    'getChecks' => [
                        new Check([
                            'status' => ComponentInterface::STATUS_FAIL
                        ])
                    ]
                ]),
                $this->makeEmpty(ComponentInterface::class, [
                    'getChecks' => [
                        new Check([
                            'status' => ComponentInterface::STATUS_WARN
                        ])
                    ]
                ]),
                $this->makeEmpty(ComponentInterface::class, [
                    'getChecks' => [
                        new Check([
                            'status' => ComponentInterface::STATUS_PASS
                        ])
                    ]
                ]),
            ],
        ]);
        (new ResponseFormatter)->format($response);
        $this->assertEquals('fail', $response->data['status']);
        $this->assertEquals(503, $response->statusCode);
    }

    public function testStatusIsWarnIfOneOfComponentsHasWarning()
    {
        $response = $this->make(Response::class, [
            'data' => [
                $this->makeEmpty(ComponentInterface::class, [
                    'getChecks' => [
                        new Check([
                            'status' => ComponentInterface::STATUS_PASS
                        ])
                    ]
                ]),
                $this->makeEmpty(ComponentInterface::class, [
                    'getChecks' => [
                        new Check([
                            'status' => ComponentInterface::STATUS_WARN
                        ])
                    ]
                ]),
                $this->makeEmpty(ComponentInterface::class, [
                    'getChecks' => [
                        new Check([
                            'status' => ComponentInterface::STATUS_PASS
                        ])
                    ]
                ]),
            ],
        ]);
        (new ResponseFormatter)->format($response);
        $this->assertEquals('warn', $response->data['status']);
        $this->assertEquals(207, $response->statusCode);
    }

    public function testStatusIsPassIfAllComponentsArePassed()
    {
        $response = $this->make(Response::class, [
            'data' => [
                $this->makeEmpty(ComponentInterface::class, [
                    'getChecks' => [
                        new Check([
                            'status' => ComponentInterface::STATUS_PASS
                        ])
                    ]
                ]),
                $this->makeEmpty(ComponentInterface::class, [
                    'getChecks' => [
                        new Check([
                            'status' => ComponentInterface::STATUS_PASS
                        ])
                    ]
                ]),
                $this->makeEmpty(ComponentInterface::class, [
                    'getChecks' => [
                        new Check([
                            'status' => ComponentInterface::STATUS_PASS
                        ])
                    ]
                ]),
            ],
        ]);
        (new ResponseFormatter)->format($response);
        $this->assertEquals('pass', $response->data['status']);
        $this->assertEquals(200, $response->statusCode);
    }
}
