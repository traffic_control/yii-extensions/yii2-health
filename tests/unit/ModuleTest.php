<?php

namespace tests\unit;

use Codeception\Test\Unit;
use trafficcontrol\yii\health\ComponentInterface;
use trafficcontrol\yii\health\Module;
use yii\base\InvalidConfigException;

class ModuleTest extends Unit
{
    public function testComponentsAreEnsuredError()
    {
        $this->expectException(InvalidConfigException::class);
        new Module('id', null, [
            'components' => 123,
        ]);
    }

    public function testComponentsAreEnsuredSuccess()
    {
        new Module('id', null, [
            'components' => [
                $this->makeEmpty(ComponentInterface::class),
            ],
        ]);
    }
}
