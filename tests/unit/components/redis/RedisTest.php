<?php

namespace tests\unit\components;

use Codeception\Stub\Expected;
use Codeception\Test\Unit;
use trafficcontrol\yii\health\ComponentInterface;
use trafficcontrol\yii\health\components\redis\Redis;
use yii\base\BaseObject;
use yii\base\InvalidConfigException;
use yii\db\Exception;
use yii\redis\Connection;

class RedisTest extends Unit
{
    public function testRedisIsEnsured()
    {
        $this->expectException(InvalidConfigException::class);

        $check = new Redis([
            'redis' => [
                'class' => BaseObject::class
            ]
        ]);
    }

    public function testErrorOnOpeningRedisConnection()
    {
        $check = new Redis([
            'redis' => $this->make(Connection::class, [
                'open' => Expected::once(function () {
                    throw new Exception('Error opening connection');
                })
            ])
        ]);

        $checks = $check->getChecks();

        $this->assertCount(1, $checks);

        $check = $checks[0];

        $this->assertEquals(ComponentInterface::STATUS_FAIL, $check->status);
    }

    public function testErrorOnCheckingActiveConnection()
    {
        $check = new Redis([
            'redis' => $this->make(Connection::class, [
                'open' => Expected::once(1),
                'getIsActive' => Expected::once(true),
            ])
        ]);

        $checks = $check->getChecks();

        $check = $checks[0];

        $this->assertEquals(ComponentInterface::STATUS_PASS, $check->status);
    }

    public function testErrorOnCheckingInactiveConnection()
    {
        $check = new Redis([
            'redis' => $this->make(Connection::class, [
                'open' => Expected::once(1),
                'getIsActive' => Expected::once(false),
            ])
        ]);

        $checks = $check->getChecks();

        $check = $checks[0];

        $this->assertEquals(ComponentInterface::STATUS_FAIL, $check->status);
    }
}