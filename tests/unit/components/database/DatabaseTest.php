<?php

namespace tests\unit\components\database;

use Codeception\Test\Unit;
use trafficcontrol\yii\health\components\database\Database;
use yii\db\Connection;
use yii\db\Exception;

class DatabaseTest extends Unit
{
    // public function testGetType()
    // {
    //     $component = new Database;
    //     $this->assertEquals('datastore', $component->getType());
    // }

    public function testGetStatusWithMisconfiguredDbComponent()
    {
        $component = new Database([
            'db' => 123,
        ]);

        $checks = $component->getChecks();

        $this->assertCount(1, $checks);

        $this->assertEquals(Database::STATUS_FAIL, $checks[0]->status);
        $this->assertEquals('Invalid data type: integer. yii\db\Connection is expected.', $checks[0]->output);
    }

    public function testGetStatusWhenCantConnectToDatabase()
    {
        $component = new Database([
            'db' => $this->makeEmpty(Connection::class, [
                'open' => function () {
                    throw new Exception('Can not connect to database. Sorry.');
                },
            ]),
        ]);

        $checks = $component->getChecks();

        $this->assertCount(1, $checks);

        $this->assertEquals(Database::STATUS_FAIL, $checks[0]->status);
        $this->assertEquals('Can not connect to database. Sorry.', $checks[0]->output);
    }

    public function testGetStatusWhenConnectionIsInactive()
    {
        $component = new Database([
            'db' => $this->makeEmpty(Connection::class, [
                'getIsActive' => false,
            ]),
        ]);

        $checks = $component->getChecks();

        $this->assertCount(1, $checks);

        $this->assertEquals(Database::STATUS_FAIL, $checks[0]->status);
        $this->assertEquals('', $checks[0]->output);
    }

    public function testGetStatusWhenConnectionIsActive()
    {
        $component = new Database([
            'db' => $this->makeEmpty(Connection::class, [
                'getIsActive' => true,
            ]),
        ]);

        $checks = $component->getChecks();

        $this->assertCount(1, $checks);

        $this->assertEquals(Database::STATUS_PASS, $checks[0]->status);
        $this->assertEquals('', $checks[0]->output);
    }
}
