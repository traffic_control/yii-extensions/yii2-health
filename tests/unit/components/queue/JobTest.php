<?php

namespace tests\unit\components\queue;

use Codeception\Stub\Expected;
use Codeception\Test\Unit;
use trafficcontrol\yii\health\components\queue\Job;
use yii\base\BaseObject;
use yii\caching\CacheInterface;
use yii\queue\sync\Queue;

class JobTest extends Unit
{
    public function testCacheIsEnsured()
    {
        $this->expectException('yii\base\InvalidConfigException');
        $this->expectExceptionMessage('Invalid data type: yii\base\BaseObject. yii\caching\CacheInterface is expected.');
        $job = new Job([
            'cache' => new BaseObject()
        ]);
    }

    public function testExecuteUpdatesLastExecutedJobCacheKey()
    {
        $cache = $this->makeEmpty(CacheInterface::class, [
            'set' => Expected::once(function ($cacheKey, $data) {
                $this->assertEquals('testKey', $cacheKey);
                $this->assertIsNumeric($data);
            })
        ]);

        $job = new Job([
            'lastExecutedJobCacheKey' => 'testKey',
            'cache' => $cache
        ]);

        $job->execute(new Queue());
    }
}
