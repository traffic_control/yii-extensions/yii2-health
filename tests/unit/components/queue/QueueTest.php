<?php

namespace tests\unit\components\queue;

use Codeception\Test\Unit;
use trafficcontrol\yii\health\components\queue\Queue;
use yii\base\InvalidConfigException;
use yii\caching\ArrayCache;
use yii\queue\sync\Queue as SyncQueue;

class QueueTest extends Unit
{
    public function testCacheIsEnsured()
    {
        $this->expectException(InvalidConfigException::class);
        new Queue([
            'cache' => 123,
        ]);
    }

    public function testGetStatusWhenNoJobsWereExecutedYet()
    {
        $queue = new SyncQueue();
        $cache = new ArrayCache();

        $component = new Queue([
            'queue' => $queue,
            'cache' => $cache,
        ]);

        $checks = $component->getChecks();

        $this->assertCount(1, $checks);

        $this->assertEquals(Queue::STATUS_WARN, $checks[0]->status);
        $this->assertEquals('No jobs were executed yet', $checks[0]->output);
    }

    public function testGetStatusWhenFailoverTimeoutPassed()
    {
        $queue = new SyncQueue();
        $cache = new ArrayCache();

        $component = new Queue([
            'queue' => $queue,
            'cache' => $cache,
        ]);

        $cache->set($component->lastExecutedJobCacheKey, time() - ($component->failoverTimeout + 1));

        $checks = $component->getChecks();

        $this->assertCount(1, $checks);
    
        $this->assertEquals(Queue::STATUS_FAIL, $checks[0]->status);
        $this->assertStringStartsWith('Seconds since last executed job:', $checks[0]->output);
    }

    public function testGetStatusWhenFailoverTimeoutNotPassed()
    {
        $queue = new SyncQueue();
        $cache = new ArrayCache();

        $component = new Queue([
            'queue' => $queue,
            'cache' => $cache,
        ]);

        $cache->set($component->lastExecutedJobCacheKey, time());

        $checks = $component->getChecks();

        $this->assertCount(1, $checks);

        $this->assertEquals(Queue::STATUS_PASS, $checks[0]->status);
        $this->assertEquals('', $checks[0]->output);
    }
}
