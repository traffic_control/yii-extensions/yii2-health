<?php

namespace tests\unit\components\filesystem;

use Codeception\Test\Unit;
use Codeception\Util\Stub;
use trafficcontrol\yii\health\ComponentInterface;
use trafficcontrol\yii\health\components\filesystem\Filesystem;

class FilesystemTest extends Unit
{
    public function testFilesystemIsFull()
    {
        $fileSystem = $this->construct(Filesystem::class, [[
            'directories' => [
                '/' => [
                    Filesystem::NAME => 'root',
                    ComponentInterface::STATUS_WARN => 80,
                    ComponentInterface::STATUS_FAIL => 90,
                ],
                '/hurr' => [
                    Filesystem::NAME => 'root',
                    ComponentInterface::STATUS_WARN => 80,
                    ComponentInterface::STATUS_FAIL => 90,
                ],
            ]
        ]],
        [
            'getFreeSpace' => Stub::consecutive(10, 20),
            'getTotalSpace' => Stub::consecutive(10, 40),
        ]);

        $checks = $fileSystem->getChecks();

        $this->assertCount(2, $checks);

        $this->assertEquals('fail', $checks[0]->status);
        $this->assertEquals('pass', $checks[1]->status);
    }
}
