<?php

namespace tests\unit\controllers;

use Codeception\Test\Unit;
use trafficcontrol\yii\health\controllers\DefaultController;
use trafficcontrol\yii\health\Module;
use trafficcontrol\yii\health\Response;
use Yii;
use yii\web\Application;
use yii\web\Request;

class DefaultControllerTest extends Unit
{
    public function testActionIndex()
    {
        Yii::$app = $this->makeEmpty(Application::class);
        $controller = new DefaultController('id', $this->make(Module::class, [
            'components' => [1,2],
        ]), [
            'request' => new Request(),
            'response' => new Response(),
        ]);
        $response = $controller->actionIndex();
        $this->assertInstanceOf(Response::class, $response);
        $this->assertEquals([1, 2], $response->data);
    }
}
