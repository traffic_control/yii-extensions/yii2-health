<?php

namespace trafficcontrol\yii\health;

/**
 * Interface ComponentInterface
 * @package trafficcontrol\yii\health
 */
interface ComponentInterface
{
    /**
     * @return Check[]
     */
    public function getChecks(): array;

    const STATUS_PASS = 'pass';
    const STATUS_FAIL = 'fail';
    const STATUS_WARN = 'warn';

    // /**
    //  * Returns type of component
    //  * @see https://tools.ietf.org/html/draft-inadarei-api-health-check-03#section-4.2
    //  * @return string
    //  */
    // public function getType(): string;

    // /**
    //  * Returns component status
    //  * @see https://tools.ietf.org/html/draft-inadarei-api-health-check-03#section-4.5
    //  * @return int
    //  */
    // public function getStatus(): int;

    // /**
    //  * Returns component output
    //  * @see https://tools.ietf.org/html/draft-inadarei-api-health-check-03#section-4.8
    //  * @return string
    //  */
    // public function getOutput(): string;
}
