<?php

namespace trafficcontrol\yii\health\components\filesystem;

use yii\helpers\ArrayHelper;

class Check extends \trafficcontrol\yii\health\Check
{
    public $name;

    public $observedValue;

    public $observedUnit = 'percent';

    /**
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            ['observedValue', 'float'],
            ['observedUnit', 'compare', 'compareValue' => 'percent'],
        ]);
    }
}
