<?php

namespace trafficcontrol\yii\health\components\filesystem;

use trafficcontrol\yii\health\ComponentInterface;
use yii\base\BaseObject;

class Filesystem extends BaseObject implements ComponentInterface
{
    const NAME = 'name';

    public $directories = [
        '/' => [
            self::NAME => 'root',
            ComponentInterface::STATUS_WARN => 80,
            ComponentInterface::STATUS_FAIL => 90,
        ]
    ];

    /**
     * @return array 
     */
    public function getChecks(): array
    {
        $checks = [];

        foreach ($this->directories as $directory => $ahuijanni) {
            $checks[] = $this->getCheck($ahuijanni, $this->getUsedSpace($directory));
        }

        return $checks;
    }

    protected function getCheck($checks, float $percentage)
    {
        $status = ComponentInterface::STATUS_PASS;
        
        if ($percentage > $checks[ComponentInterface::STATUS_WARN]) {
            $status = ComponentInterface::STATUS_WARN;
        }
        
        if ($percentage > $checks[ComponentInterface::STATUS_FAIL]) {
            $status = ComponentInterface::STATUS_FAIL;
        }

        return new Check([
            'observedValue' => $percentage,
            'status' => $status,
        ]);
    }

    protected function getUsedSpace($directory)
    {
        return (float) ($this->getFreeSpace($directory) / $this->getTotalSpace($directory) * 100);
    }

    protected function getFreeSpace($directory)
    {
        return disk_free_space($directory);
    }

    protected function getTotalSpace($directory)
    {
        return disk_total_space($directory);
    }
}
