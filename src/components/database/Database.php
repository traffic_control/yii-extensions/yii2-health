<?php

namespace trafficcontrol\yii\health\components\database;

use trafficcontrol\yii\health\ComponentInterface;
use yii\base\BaseObject;
use yii\base\InvalidConfigException;
use yii\db\Connection;
use yii\db\Exception;
use yii\di\Instance;

/**
 * Class Database
 * // TODO description
 * @package trafficcontrol\yii\health\components
 */
class Database extends BaseObject implements ComponentInterface
{
    /**
     * Database component
     * @var Connection|array|string
     */
    public $db = 'db';

    /**
     * @return array 
     */
    public function getChecks(): array
    {
        $check = new Check;

        try {
            /**
             * @var Connection $database 
             */
            $database = Instance::ensure($this->db, Connection::class);
        } catch (InvalidConfigException $e) {
            $check->output = $e->getMessage();
            $check->status = ComponentInterface::STATUS_FAIL;
            return [$check];
        }
        
        try {
            $database->open();
        } catch (Exception $e) {
            $check->output = $e->getMessage();
            $check->status = ComponentInterface::STATUS_FAIL;
            return [$check];
        }
        
        $check->status = $database->getIsActive() ? self::STATUS_PASS : self::STATUS_FAIL;

        return [$check];
    }
}
