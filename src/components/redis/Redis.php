<?php

namespace trafficcontrol\yii\health\components\redis;

use Exception;
use trafficcontrol\yii\health\ComponentInterface;
use trafficcontrol\yii\health\components\database\Check;
use yii\base\BaseObject;
use yii\base\InvalidConfigException;
use yii\di\Instance;
use yii\redis\Connection;

class Redis extends BaseObject implements ComponentInterface
{
    /**
     * Database component
     *
     * @var Connection|array|string
     */
    public $redis = 'redis';

    /**
     * @inheritDoc
     * @throws     InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->redis = Instance::ensure($this->redis, Connection::class);
    }

    /**
     * @return Check[]
     */
    public function getChecks(): array
    {
        $check = new Check();

        try {
            $this->redis->open();
        } catch (Exception $e) {
            $check->output = $e->getMessage();
            $check->status = ComponentInterface::STATUS_FAIL;
            return [$check];
        }

        $check->status = $this->redis->getIsActive() ? self::STATUS_PASS : self::STATUS_FAIL;

        return [$check];
    }
}
