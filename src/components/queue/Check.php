<?php

namespace trafficcontrol\yii\health\components\queue;

use yii\helpers\ArrayHelper;

class Check extends \trafficcontrol\yii\health\Check
{
    public $output;

    /**
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            ['output', 'text'],
        ]);
    }
}
