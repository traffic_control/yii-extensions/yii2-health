<?php

namespace trafficcontrol\yii\health\components\queue;

use DateTime;
use DateTimeInterface;
use trafficcontrol\yii\health\ComponentInterface;
use yii\base\BaseObject;
use yii\base\InvalidConfigException;
use yii\caching\CacheInterface;
use yii\di\Instance;
use yii\queue\Queue as BaseQueue;

/**
 * Class Queue
 * // TODO description
 * @package trafficcontrol\yii\health\components
 */
class Queue extends BaseObject implements ComponentInterface
{
    /**
     * @var CacheInterface|array|string
     */
    public $cache = 'cache';

    /**
     * @var Queue|array|string
     */
    public $queue = 'queue';

    /**
     * Cache key for last executed job timestamp
     * @var string
     */
    public $lastExecutedJobCacheKey = self::class;

    /**
     * Second from last executed job for queue to be reported as failed
     * @var int
     */
    public $failoverTimeout = 300;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->cache = Instance::ensure($this->cache, CacheInterface::class);
        $this->queue = Instance::ensure($this->queue, BaseQueue::class);
    }

    public function getChecks(): array
    {
        $check = new Check([
            'status' => self::STATUS_PASS,
        ]);

        $this->queue->push(new Job([
            'cache' => $this->cache,
            'lastExecutedJobCacheKey' => $this->lastExecutedJobCacheKey,
        ]));

        $lastExecutedAt = $this->getLastExecutedAt();
        if ($lastExecutedAt === null) {
            $check->output = 'No jobs were executed yet';
            $check->status = self::STATUS_WARN;
        }

        if ($lastExecutedAt) {
            $secondsSinceLastJob = (new DateTime())->getTimestamp() - $lastExecutedAt->getTimestamp();
            if ($secondsSinceLastJob >= $this->failoverTimeout) {
                $check->output = 'Seconds since last executed job: ' . $secondsSinceLastJob;
                $check->status =  self::STATUS_FAIL;
            }
        }

        return [$check];
    }

    private function getLastExecutedAt(): ?DateTimeInterface
    {
        $lastExecutedAt = DateTime::createFromFormat('U', $this->cache->get($this->lastExecutedJobCacheKey));
        return $lastExecutedAt ? $lastExecutedAt : null;
    }
}
