<?php

namespace trafficcontrol\yii\health\components\queue;

use yii\base\BaseObject;
use yii\caching\CacheInterface;
use yii\di\Instance;
use yii\queue\JobInterface;
use yii\queue\Queue;

class Job extends BaseObject implements JobInterface
{
    /**
     * @var CacheInterface|array|string
     */
    public $cache = 'cache';

    public $lastExecutedJobCacheKey;

    /**
     * @inheritDoc
     * @throws     InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->cache = Instance::ensure($this->cache, CacheInterface::class);
    }

    /**
     * @param Queue $queue
     *
     * @return void|mixed
     */
    public function execute($queue)
    {
        $this->cache->set($this->lastExecutedJobCacheKey, time());
    }
}
