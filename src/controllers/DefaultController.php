<?php

namespace trafficcontrol\yii\health\controllers;

use trafficcontrol\yii\health\Response;
use yii\web\Controller;

/**
 * Class DefaultController
 * @package trafficcontrol\yii\health\controllers
 */
class DefaultController extends Controller
{
    /**
     * @return Response
     */
    public function actionIndex()
    {
        return new Response([
            'data' => $this->module->components,
        ]);
    }
}
