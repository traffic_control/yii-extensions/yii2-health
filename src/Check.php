<?php

namespace trafficcontrol\yii\health;

use DateTime;
use trafficcontrol\yii\health\ComponentInterface;
use yii\base\Model;

class Check extends Model
{
    public $componentType;

    public $time;

    public $status;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['time', 'default', 'value' => function () {
                return (new DateTime())->format(DateTime::ISO8601);
            }],
            ['status', 'required'],
            ['status', 'in', 'range' => [ComponentInterface::STATUS_PASS, ComponentInterface::STATUS_WARN, ComponentInterface::STATUS_FAIL]],
        ];
    }
}
