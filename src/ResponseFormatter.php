<?php

namespace trafficcontrol\yii\health;

use DateTime;
use trafficcontrol\yii\health\ComponentInterface;
use yii\web\JsonResponseFormatter;

/**
 * Class ResponseFormatter
 * @package trafficcontrol\yii\health
 */
class ResponseFormatter extends JsonResponseFormatter
{
    const CONTENT_TYPE_HEALTH = 'application/health+json';

    /**
     * @inheritdoc
     */
    public $contentType = self::CONTENT_TYPE_HEALTH;

    /**
     * @inheritDoc
     */
    public function format($response)
    {
        $data = [
            'status' => 'pass',
            'checks' => [],
        ];

        $globalStatus = ComponentInterface::STATUS_PASS;

        /**
         * @var string $componentName
         * @var ComponentInterface $component
         */
        foreach ($response->data as $componentName => $component) {
            $data['checks'][$componentName] = $component->getChecks();
        }

        foreach ($data['checks'] as $checks) {
            foreach ($checks as $check) {
                if ($check->status === ComponentInterface::STATUS_FAIL) {
                    $globalStatus = ComponentInterface::STATUS_FAIL;
                    $response->setStatusCode(503);
                    break 2; //No need to observe further
                }
                if ($check->status === ComponentInterface::STATUS_WARN) {
                    $globalStatus = ComponentInterface::STATUS_WARN;
                    $response->setStatusCode(207);
                }
            }
        }

        $data['status'] = $globalStatus;

        $response->data = $data;
        parent::format($response);
    }
}
